const csv = require('csv-parser');
const fs = require('fs');
const path = require("path")
var express = require('express');
const { exec } = require('child_process');

var app = express();

const getAllFiles = function(dirPath, arrayOfFiles) {
  files = fs.readdirSync(dirPath)
  arrayOfFiles = arrayOfFiles || [];
  files.forEach(function(file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
    } else {
      arrayOfFiles.push(path.join(__dirname, dirPath, "/", file))
    }
  })
  return arrayOfFiles
}

function xls2csv(res, file){
	debugger;
	exec("convert.bat", (error, stdout, stderr) => {
		debugger;
	// exec("pwd", (error, stdout, stderr) => {
		if (error) {
			console.log(`error: ${error.message}`);
			return;
		}
		if (stderr) {
			console.log(`stderr: ${stderr}`);
			return;
		}
		console.log(`stdout: ${stdout}`);
		debugger;
		fs.rename('public/Sheet3.csv', 'public/cems_data.csv', function(error) {
		// exec("ls -lh public/cems_data.csv", (error, stdout, stderr) => {
			debugger;
			if (error) {
				console.log('ls error: ' + error);
			} else {
				console.log("ls stdout: " + stdout);
				var csv_lines = [];
				fs.createReadStream('public/cems_data.csv')
					.pipe(csv())
					.on('data', (row) => {
						console.log(row);
						csv_lines.push(row);
				})
				.on('end', () => {
					debugger;
					console.log('CSV file successfully processed');
					res.render('pages/index', {
						csv_lines: csv_lines
					});
				});
			};
		})
	})
}

app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  console.log("getting");
  var rv = getAllFiles('public/ReportsExported', []);
  rv.reverse();
  rv.forEach(function(file, pos){
      rv[pos] = file.replace("~$","");
  });
  var latest_file = rv[0];
  console.log("latest: " + latest_file);
  create_parameter_file(res, latest_file);
});

function create_parameter_file(res, latest_file){
	debugger;
	fs.readFile('convert_template.txt', 'utf8' , (err, data) => {
		debugger;
		if (err) {
			console.error(err)
			return
		}
		data = data.replace("%1", latest_file);
		fs.writeFile('convert.ini', data, function (err) {
			debugger;
			if (err) return console.log(err);
			xls2csv(res, latest_file);
		});
	})
}

app.get('/about', function(req, res) {
    res.render('pages/about');
});

var port = 80;
app.listen(port);
console.log('server is on-line at port '+port);